/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of Talky                                                  *
 *                                                                             *
 * Talky is free software: you can redistribute it and/or modify               *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * Talky is distributed in the hope that it will be useful,                    *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "Settings.h"

using namespace boost::property_tree::xml_parser;

Settings::Settings(UnicodeString Filename) :
	m_filename(Filename)
{
	m_crypt = new Cryptography();
}

Settings::~Settings()
{
	if (m_settingsFile.is_open())
		m_settingsFile.close();

	delete m_crypt;
}

bool Settings::settingsExists()
{
	return FileExists(m_filename);
}

bool Settings::load()
{
	if (!settingsExists())
		return false;

	m_settingsFile.open(m_filename.w_str(), ios::in);

	// TODO: log this
	if (!m_settingsFile.good())
		return false;

	try
	{
		boost::property_tree::xml_parser::read_xml(m_settingsFile, m_propertyTree);
	}
	catch(...)
	{
		//Cannot load log file
		//TODO: log this
		m_settingsFile.close();
		return false;
	}

	m_settingsFile.close();
	return true;
}

void Settings::save()
{
	m_settingsFile.open(m_filename.w_str(), ios::out);

	// TODO: log this
	if (!m_settingsFile.good())
		return;

	boost::property_tree::xml_writer_settings<char> settings('\t', 1);
	boost::property_tree::xml_parser::write_xml(m_settingsFile, m_propertyTree, settings);

	m_settingsFile.close();
}

void Settings::clear()
{
	if (!settingsExists())
		return;

	DeleteFile(m_filename);
   	m_propertyTree.clear();
}

void Settings::addString(std::string Name, UnicodeString Value)
{
	m_propertyTree.put(Name, AnsiString(Value).c_str());
}

void Settings::addInt(std::string Name, int Value)
{
	m_propertyTree.put(Name, Value);
}

void Settings::addBool(std::string Name, bool Value)
{
	m_propertyTree.put(Name, Value);
}

UnicodeString Settings::getString(std::string Name)
{
	return std::string(m_propertyTree.get<std::string>(Name)).c_str();
}

int Settings::getInt(std::string Name)
{
	return m_propertyTree.get(Name, -1);
}

bool Settings::getBool(std::string Name)
{
	return m_propertyTree.get(Name, false);
}

