/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of Talky                                                  *
 *                                                                             *
 * Talky is free software: you can redistribute it and/or modify               *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * Talky is distributed in the hope that it will be useful,                    *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once

#include <vcl.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <core\Utils\Cryptography.h>

class Settings
{
private:
	UnicodeString m_filename;
	boost::property_tree::ptree m_propertyTree;
	fstream m_settingsFile;
	Cryptography* m_crypt;
public:
	Settings(UnicodeString Filename);
	virtual ~Settings();

	bool settingsExists();
	bool load();
	void save();
	void clear();

	void addString(std::string Name, UnicodeString Value);
	void addInt(std::string Name, int Value);
	void addBool(std::string Name, bool Value);

	UnicodeString getString(std::string Name);
	int getInt(std::string Name);
	bool getBool(std::string Name);
};
