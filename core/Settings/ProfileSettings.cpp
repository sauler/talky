/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of Talky                                                  *
 *                                                                             *
 * Talky is free software: you can redistribute it and/or modify               *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * Talky is distributed in the hope that it will be useful,                    *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "ProfileSettings.h"

ProfileSettings::ProfileSettings(UnicodeString Filename, UnicodeString ProfileName) :
	Settings(Filename), m_profileName(ProfileName)
{
	if (!settingsExists())
	{
		setProfileName(m_profileName);
		save();
	}
	else
		load();
}

ProfileSettings::~ProfileSettings()
{

}

void ProfileSettings::setPassword(UnicodeString Password)
{
	m_password = Password;
	addString("Password", m_password);
}

UnicodeString ProfileSettings::getPassword()
{
	m_password = getString("Password");
	return m_password;
}

void ProfileSettings::setProfileName(UnicodeString ProfileName)
{
	m_profileName = ProfileName;
	addString("Name", m_profileName);
}

UnicodeString ProfileSettings::getProfileName()
{
	m_profileName = getString("Name");
	return m_profileName;
}

void ProfileSettings::setTheme(UnicodeString ThemeName)
{
	m_theme = ThemeName;
	addString("Theme", m_theme);
}

UnicodeString ProfileSettings::getTheme()
{
	m_theme = getString("Theme");
	return m_theme;
}

void ProfileSettings::setUsePassword(bool UsePassword)
{
	m_usePassword = UsePassword;
	addBool("HasPassword", m_usePassword);
}

bool ProfileSettings::getUsePassword()
{
	m_usePassword = getBool("HasPassword");
	return m_usePassword;
}