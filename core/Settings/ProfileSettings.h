/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of Talky                                                  *
 *                                                                             *
 * Talky is free software: you can redistribute it and/or modify               *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * Talky is distributed in the hope that it will be useful,                    *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once

#include "Settings.h"

class ProfileSettings : public Settings
{
private:
	UnicodeString m_password;
	UnicodeString m_profileName;
	UnicodeString m_theme;
	bool m_usePassword;
public:
	ProfileSettings(UnicodeString Filename, UnicodeString ProfileName);
	~ProfileSettings();

	void setPassword(UnicodeString Password);
	UnicodeString getPassword();

	void setProfileName(UnicodeString ProfileName);
	UnicodeString getProfileName();

	void setTheme(UnicodeString ThemeName);
	UnicodeString getTheme();

	void setUsePassword(bool UsePassword);
	bool getUsePassword();
};
