/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of Talky                                                  *
 *                                                                             *
 * Talky is free software: you can redistribute it and/or modify               *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * Talky is distributed in the hope that it will be useful,                    *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "Profile.h"

#include <core\Settings\ProfileSettings.h>

Profile::Profile(UnicodeString Path, UnicodeString Name) :
	m_path(Path), m_name(Name)
{
	m_path = m_path + "\\" + Name;
	m_accountsPath = m_path + "\\Accounts";
	m_pluginsPath = m_path + "\\Plugins";
	m_smileys = m_path + "\\Smileys";
	m_themesPath = m_path + "\\Themes";

	if (!DirectoryExists(m_path))
	{
		CreateDir(m_path);
		CreateDir(m_accountsPath);
		CreateDir(m_pluginsPath);
		CreateDir(m_smileys);
		CreateDir(m_themesPath);
	}
   m_settingsFilePath = m_path + "\\Profile.xml";
   m_settings = new ProfileSettings(m_settingsFilePath, m_name);
}

Profile::~Profile()
{
	delete m_settings;
}

/* Function returns a profile name */
UnicodeString Profile::getName()
{
 	return m_name;
}

/* Function returns a profile settings */
ProfileSettings* Profile::getSettings()
{
	return m_settings;
}

/* Function loads profile settings. Return true if successed */
bool Profile::loadSettings()
{
	return m_settings->load();
}

/* Function saves profile settings */
void Profile::saveSettings()
{
	m_settings->save();
}
