/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of Talky                                                  *
 *                                                                             *
 * Talky is free software: you can redistribute it and/or modify               *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * Talky is distributed in the hope that it will be useful,                    *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once

#include <vcl.h>

class ProfileSettings;

class Profile
{
private:
	UnicodeString m_name;
	UnicodeString m_path;
	UnicodeString m_accountsPath;
	UnicodeString m_pluginsPath;
	UnicodeString m_settingsFilePath;
	UnicodeString m_smileys;
	UnicodeString m_themesPath;

	ProfileSettings* m_settings;
public:
	Profile(UnicodeString Path, UnicodeString Name);
	~Profile();

	UnicodeString getName();
	ProfileSettings* getSettings();
	bool loadSettings();
	void saveSettings();
};
