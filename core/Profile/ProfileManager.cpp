/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of Talky                                                  *
 *                                                                             *
 * Talky is free software: you can redistribute it and/or modify               *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * Talky is distributed in the hope that it will be useful,                    *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "ProfileManager.h"

#include "PathManager.h"
#include "Profile.h"

ProfileManager::ProfileManager()
{
	m_profilesPath = PathManager::instance()->getProfilesDir();
}

ProfileManager::~ProfileManager()
{
}

/* Function creates new profile. Returns true when successed, false if profile already exists. */
bool ProfileManager::createProfile(UnicodeString Name)
{
	Profile* ToCreate;
	ToCreate = m_profiles[Name];
	if (ToCreate != nullptr)
		return false;
	else
	{
		m_profiles[Name] = new Profile(m_profilesPath, Name);
		m_lastAddedProfile = Name;
		return true;
	}
}

/* Function removes profile. Returns true when successed, false if profile not exists. */
bool ProfileManager::removeProfile(UnicodeString Name)
{
	Profile* ToRemove;
	ToRemove = m_profiles[Name];
	if (ToRemove != nullptr)
	{
		delete ToRemove;
		return true;
	}
	else
		return false;
}

/* Function removes all existing profiles. */
void ProfileManager::removeAllProfiles()
{
	for (m_profiles_it it = m_profiles.begin(); it != m_profiles.end(); ++it)
		removeProfile(it->second->getName());
}

/* Function returns last added profile. If no profile was not created function return nullptr */
Profile* ProfileManager::getLastAddedProfile()
{
	Profile* profile;
	profile = m_profiles[m_lastAddedProfile];
	if (profile == nullptr)
		return nullptr;
	else
		return profile;
}

/* Function returns profile with the specifed name. If profile not exists function return nullptr */
Profile* ProfileManager::getProfile(UnicodeString Name)
{
	Profile* profile;
	profile = m_profiles[Name];
	if (profile == nullptr)
		return nullptr;
	else
		return profile;
}

/* Function returns profiles path */
UnicodeString ProfileManager::getProfilesPath()
{
	return m_profilesPath;
}

/* Function lists names of all existing profiles to given TStrings */
void ProfileManager::getListOfAllProfiles(TStrings* Names)
{
	for (m_profiles_it it = m_profiles.begin(); it != m_profiles.end(); ++it)
		Names->Add(it->second->getName());
}

/* Function returns count of profiles */
int ProfileManager::getProfilesCount()
{
	return m_profiles.size();
}
