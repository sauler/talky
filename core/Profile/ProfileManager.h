/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of Talky                                                  *
 *                                                                             *
 * Talky is free software: you can redistribute it and/or modify               *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * Talky is distributed in the hope that it will be useful,                    *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once

#include <map>
#include <vcl.h>

class Profile;

class ProfileManager
{
private:
	typedef std::map<UnicodeString, Profile*>::iterator m_profiles_it;
	std::map<UnicodeString, Profile*> m_profiles;
	UnicodeString m_lastAddedProfile;

	UnicodeString m_profilesPath;
public:
	ProfileManager();
	~ProfileManager();

	bool createProfile(UnicodeString Name);
	bool removeProfile(UnicodeString Name);
	void removeAllProfiles();
	Profile* getLastAddedProfile();
	Profile* getProfile(UnicodeString Name);
	UnicodeString getProfilesPath();
	void getListOfAllProfiles(TStrings* Names);
	int getProfilesCount();
};
