/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of Talky                                                  *
 *                                                                             *
 * Talky is free software: you can redistribute it and/or modify               *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * Talky is distributed in the hope that it will be useful,                    *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "PathManager.h"

#include <windows.h>

#include <core/Constants.h>

PathManager* PathManager::m_instance = nullptr;

/* Function returns instance of PathManager object */
PathManager* PathManager::instance()
{
	if (m_instance == nullptr)
		m_instance = new PathManager();

	return m_instance;
}

/* Function returns %localappdata% path */
UnicodeString PathManager::getLocalAppdataPath()
{
	UnicodeString Result;
	SHGetFolderPath(nullptr, CSIDL_LOCAL_APPDATA, nullptr, 0, Result.c_str());
	return Result;
}

/* Function returns directory to store profiles. If directory not exists this function will create it */
UnicodeString PathManager::getProfilesDir()
{
	UnicodeString Result;
	Result = getLocalAppdataPath() + "\\" + App::Name + "\\Profiles";
	if (!DirectoryExists(Result))
		ForceDirectories(Result);
	return Result;
}
