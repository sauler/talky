/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of Talky                                                  *
 *                                                                             *
 * Talky is free software: you can redistribute it and/or modify               *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * Talky is distributed in the hope that it will be useful,                    *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once

#include <vcl.h>

#include <uTPLb_CryptographicLibrary.hpp>
#include <uTPLb_Codec.hpp>

class Cryptography
{
private:
	TCodec* m_codec;
	TCryptographicLibrary* m_crypt;
public:
	Cryptography();
	~Cryptography();

	void setPassword(UnicodeString Password);

	UnicodeString encryptString(UnicodeString Text);
	UnicodeString decryptString(UnicodeString EncryptedText);

	void encryptFile(UnicodeString Filename, UnicodeString EncryptedFilename);
	void decryptFile(UnicodeString EncryptedFilename, UnicodeString DecryptedFilename);

	void encryptStream(TStream PlainStream, TStream EncryptedStream);
	void decryptStream(TStream EncryptedStream, TStream DecryptedStream);
};