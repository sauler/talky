/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of Talky                                                  *
 *                                                                             *
 * Talky is free software: you can redistribute it and/or modify               *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * Talky is distributed in the hope that it will be useful,                    *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "Cryptography.h"

#include <uTPLb_Constants.hpp>

Cryptography::Cryptography()
{
	m_codec = new TCodec(0);
	m_crypt = new TCryptographicLibrary(0);
	m_codec->CryptoLibrary = m_crypt;
	m_codec->StreamCipherId = BlockCipher_ProgId;
	m_codec->BlockCipherId  = "native.AES-256";
	m_codec->ChainModeId    = CBC_ProgId;
}

Cryptography::~Cryptography()
{
	delete m_codec;
	delete m_crypt;
}

/* Function sets password used to encrypting/decrypting. Set before encrypt/decrypt! */
void Cryptography::setPassword(UnicodeString Password)
{
	m_codec->Password = Password;
}

/* Function encrypt a given Text */
UnicodeString Cryptography::encryptString(UnicodeString Text)
{
	UnicodeString Result;

	m_codec->EncryptAnsiString(Text, Result);
	m_codec->Reset();
	return Result;
}

/* Function decrypt a given EncryptedText */
UnicodeString Cryptography::decryptString(UnicodeString EncryptedText)
{
	UnicodeString Result;

	m_codec->DecryptAnsiString(Result, EncryptedText);
	m_codec->Reset();
	return Result;
}

/* Function encrypt a given file and save encrypted version in path given in EncryptedFilename */
void Cryptography::encryptFile(UnicodeString Filename, UnicodeString EncryptedFilename)
{
	m_codec->EncryptFileW(Filename, EncryptedFilename);
	m_codec->Reset();
}

/* Function decrypt a given EncryptedFilename and save decrypted version in path given in DecryptedFilename */
void Cryptography::decryptFile(UnicodeString EncryptedFilename, UnicodeString DecryptedFilename)
{
	m_codec->DecryptFileW(DecryptedFilename, EncryptedFilename);
	m_codec->Reset();
}

/* Function encrypt a given PlainStream and save it into EncryptedStream */
void Cryptography::encryptStream(TStream PlainStream, TStream EncryptedStream)
{
	m_codec->EncryptStream(&PlainStream, &EncryptedStream);
	m_codec->Reset();
}

/* Function decrypt a given EncryptedStream and save it into DecryptedStream */
void Cryptography::decryptStream(TStream EncryptedStream, TStream DecryptedStream)
{
	m_codec->DecryptStream(&DecryptedStream, &EncryptedStream);
	m_codec->Reset();
}
