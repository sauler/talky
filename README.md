# Talky
Talky is multi protocol instant messaging app.

### Requirements
If you want to compile this plugin you need:

* Embarcadero RAD Studio DX10 Seattle
* LockBox3 for C++ Builder

### Issues
If you find error in this app please report it by writing directly to author of the plugin (preferred contact through Jabber)

### Contact with author
Author of this app is Rafał Babiarz. You may to contact with him using e-mail (sauler1995@gmail.com) or Jabber (sauler@jix.im).

### License
Talky is licensed under [GNU General Public License 3](http://www.gnu.org/copyleft/gpl.html).

~~~~
Talky
Copyright © 2016  Rafał Babiarz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
~~~~